package com.example.SchnittStelle;
import java.time.LocalDate;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;




@Data @Getter public class Ticket {


    @Setter private boolean inetstat;
    @Setter private String hardware;
    @Setter private String anschrift;
    @Setter private String email;
    @Setter private String vorname;
    @Setter private String nachname;
    @Setter private LocalDate kursanfang;
    @Setter private LocalDate kursende;

	@JsonFormat(pattern = "dd/MM/yyyy")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
    @Setter private String versandreferemz;
    @Setter private LocalDate versanddatum;
    @Setter private String versandstatus;

    @Setter private String problembeschreibung;
    @Setter private String internerbearbeiter;


}
