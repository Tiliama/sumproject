package com.example.SchnittStelle;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

public class TicketRestController {

  private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

  @RequestMapping("/getWithRequestParam")
  public List<Object> getWithRequestParam(@RequestParam(value = "ticket") String Ticket)
      throws IOException {
	  @RequestBody final Ticket Irgenjemand =
        new ObjectMapper().setDateFormat(simpleDateFormat).readValue(Ticket, Ticket.class);
    return Arrays.asList(
        Irgenjemand.getInetstat(),
        Irgenjemand.getHardware(),
        Irgenjemand.getAnschrift(),
        Irgenjemand.getEmail(),
        Irgenjemand.getVorname(),
		Irgenjemand.getNachname(),
		Irgenjemand.getKursanfang(),
		Irgenjemand.getKursende(),
		Irgenjemand.getVersandreferemz(),
		Irgenjemand.getVersanddatum(),
		Irgenjemand.getVersandstatus(),
		Irgenjemand.getProblembeschreibung(),
		Irgenjemand.getInternerbearbeiter()
		);
}
		  @RequestMapping("/getWithoutRequestParam")
  public List<Object> getWithoutRequestParam(Ticket ticket) {
    return Arrays.asList(
        ticket.getInetstat(),
        ticket.getHardware(),
        ticket.getAnschrift(),
        ticket.getEmail(),
        ticket.getVorname(),
		ticket.getNachname(),
		ticket.getKursanfang(),
		ticket.getKursende(),
		ticket.getVersandreferemz(),
		ticket.getVersanddatum(),
		ticket.getVersandstatus(),
		ticket.getProblembeschreibung(),
		ticket.getInternerbearbeiter()
		);
  }
 
 
   @RequestMapping(value = "/getWithMultipleParameters")
  public List<Object> getWithMultipleParameters(
		   @RequestBody final Ticket ticket, @RequestParam(value = "Ticket") String Ticket) {
    return Arrays.asList(
        ticket.getInetstat(),
        ticket.getHardware(),
        ticket.getAnschrift(),
        ticket.getEmail(),
        ticket.getVorname(),
		ticket.getNachname(),
		ticket.getKursanfang(),
		ticket.getKursende(),
		ticket.getVersandreferemz(),
		ticket.getVersanddatum(),
		ticket.getVersandstatus(),
		ticket.getProblembeschreibung(),
		ticket.getInternerbearbeiter()
        );
  }
 
 
   @RequestMapping("/getWithMultipleRequestParams")
	public List<Object> getWithMultipleRequestParams(
      @RequestParam(value = "Ticket") String Ticket,
      @RequestParam(value = "Hardware") String Hardware)
      throws IOException {
    final Ticket Irgenjemand = null;
	   new ObjectMapper().setDateFormat(simpleDateFormat).readValue(Ticket, Ticket.class);
    return Arrays.asList(
			Irgenjemand.getInetstat(),
			Irgenjemand.getHardware(),
			Irgenjemand.getAnschrift(),
			Irgenjemand.getEmail(),
			Irgenjemand.getVorname(),
			Irgenjemand.getNachname(),
			Irgenjemand.getKursanfang(),
			Irgenjemand.getKursende(),
			Irgenjemand.getVersandreferemz(),
			Irgenjemand.getVersanddatum(),
			Irgenjemand.getVersandstatus(),
			Irgenjemand.getProblembeschreibung(),
			Irgenjemand.getInternerbearbeiter()
	);

  }
}