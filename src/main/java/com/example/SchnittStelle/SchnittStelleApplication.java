package com.example.SchnittStelle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchnittStelleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchnittStelleApplication.class, args);
	}

}
